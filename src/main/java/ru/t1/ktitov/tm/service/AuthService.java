package ru.t1.ktitov.tm.service;

import ru.t1.ktitov.tm.api.service.IAuthService;
import ru.t1.ktitov.tm.api.service.IUserService;
import ru.t1.ktitov.tm.enumerated.Role;
import ru.t1.ktitov.tm.exception.field.EmptyLoginException;
import ru.t1.ktitov.tm.exception.field.EmptyPasswordException;
import ru.t1.ktitov.tm.exception.user.AccessDeniedException;
import ru.t1.ktitov.tm.exception.user.IncorrectLogPassException;
import ru.t1.ktitov.tm.exception.user.PermissionException;
import ru.t1.ktitov.tm.model.User;
import ru.t1.ktitov.tm.util.HashUtil;

import java.util.Arrays;

public final class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public User registry(final String login, final String password, final String email) {
        return userService.create(login, password, email);
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new IncorrectLogPassException();
        final String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new IncorrectLogPassException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        final User user = userService.findOneById(userId);
        if (user == null) throw new AccessDeniedException();
        return user;
    }

    @Override
    public void checkRoles(Role[] roles) {
        if (roles == null) return;
        final User user = getUser();
        final Role role = user.getRole();
        if (role == null) throw new PermissionException();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }

}
