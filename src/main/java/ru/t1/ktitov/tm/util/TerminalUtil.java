package ru.t1.ktitov.tm.util;

import ru.t1.ktitov.tm.exception.AbstractException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() {
        try {
            final String value = nextLine();
            return Integer.parseInt(value);
        } catch (final Exception e) {
            throw new InvalidNumberException();
        }
    }

    final class InvalidNumberException extends AbstractException {
        public InvalidNumberException() {
            super("Error! Value is not a number.");
        }
    }

}
